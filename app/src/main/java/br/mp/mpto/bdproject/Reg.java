package br.mp.mpto.bdproject;

public class Reg {
    private int id;
    private String name;
    private String imagePath;
    private float rating;

    public Reg(String name, String imagePath, float rating) {
        this.name = name;
        this.imagePath = imagePath;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}

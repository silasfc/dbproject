package br.mp.mpto.bdproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

public class BDHandler extends SQLiteOpenHelper {

    private String[] scripts = new String[2];
    private Context context;

    public BDHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        this.context = context;

        scripts[0] = "CREATE TABLE `reg`" +
                "(" +
                "`id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                "`name` TEXT NOT NULL," +
                "`imagePath` TEXT," +
                "`rating` FLOAT NOT NULL" +
                ");";
        scripts[1] = "DROP TABLE IF EXISTS `reg`";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(scripts[0]);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(scripts[1]);
    }

    public void insert(Reg reg) {
        ContentValues values = new ContentValues();
        values.put("name", reg.getName());
        values.put("imagePath", reg.getImagePath());
        values.put("rating", reg.getRating());

        SQLiteDatabase db = getWritableDatabase();
        db.insert("reg", null, values);
        Toast.makeText(context, "Insertion sucessfully", Toast.LENGTH_SHORT).show();
    }

    public ArrayList<Reg> getRegs() {
        ArrayList<Reg> list = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(
            "reg",
            new String[]{"name", "imagePath", "rating"},
            null,
            null,
            null,
            null,
            null
        );

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                list.add(
                    new Reg(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getFloat(2)
                    )
                );
            }
        }

        cursor.close();

        return list;
    }
}

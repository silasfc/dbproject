package br.mp.mpto.bdproject;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class List extends AppCompatActivity {

    RecyclerView recyclerView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        recyclerView = findViewById(R.id.list_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        BDHandler bdHandler = new BDHandler(this, "bd", null, 1);
        updateRecyclerView(bdHandler.getRegs());
    }

    public void handleFloatButton(View button) {
        Intent intent = new Intent(this, Form.class);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            BDHandler bdHandler = new BDHandler(
                this,
                "bd",
                null,
                1
            );
            updateRecyclerView(bdHandler.getRegs());
        }
    }

    private void updateRecyclerView(ArrayList<Reg> list) {
        // Atualiza a lista caso a operacao seja ok
        // Define o Adapter da RecyclerView
        this.recyclerView.setAdapter(new Adapter(this, list));
    }
}

package br.mp.mpto.bdproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

public class Form extends AppCompatActivity {

    ImageView imageField = null;

    Uri imagePath = null;
    File file = null;

    BDHandler bdHandler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
    }

    public void handleCamera(View button) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        String name = "image_" + System.currentTimeMillis();
        File path = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        try {
            file = File.createTempFile(name, "jpg", path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        imagePath = FileProvider.getUriForFile(this, "br.mp.mpto.bdproject.fileprovider", file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imagePath);

        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            imageField = findViewById(R.id.form_image);
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            imageField.setImageBitmap(bitmap);
        }
    }

    public void handleSave(View button) {
        BDHandler bdHandler = new BDHandler(this, "bd", null, 1);

        // Busca as referencias de objetos visuais
        TextView nameField = findViewById(R.id.form_name);
        RatingBar ratingBarField = findViewById(R.id.form_ratingBar);

        // Instancia um registro
        Reg reg = new Reg(
            nameField.getText().toString(),
            file.getAbsolutePath(),
            ratingBarField.getRating()
        );

        // Insere o registro no Banco
        bdHandler.insert(reg);

        this.setResult(Activity.RESULT_OK);

        // Finaliza a tela
        finish();
    }
}

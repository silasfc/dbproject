package br.mp.mpto.bdproject;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class RegHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private ImageView image;
    private RatingBar rating;

    public RegHolder(View cell) {
        super(cell);
        this.name = cell.findViewById(R.id.cell_name);
        this.image = cell.findViewById(R.id.cell_image);
        this.rating = cell.findViewById(R.id.cell_ratingBar);
    }

    public TextView getName() {
        return name;
    }

    public ImageView getImage() {
        return image;
    }

    public RatingBar getRating() {
        return rating;
    }
}

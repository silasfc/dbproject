package br.mp.mpto.bdproject;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<RegHolder> {
    private Context context;
    private ArrayList<Reg> list;

    public Adapter(Context context, ArrayList<Reg> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RegHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cell = LayoutInflater.from(context).inflate(R.layout.layout_cell, parent, false);
        RegHolder holder = new RegHolder(cell);

        return holder;
    }

    @Override
    public void onBindViewHolder(RegHolder holder, int position) {
        Reg reg = list.get(position);

        holder.getName().setText(reg.getName());
        holder.getImage().setImageBitmap(BitmapFactory.decodeFile(reg.getImagePath()));
        holder.getRating().setRating(reg.getRating());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
